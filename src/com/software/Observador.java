package com.software;

public abstract class Observador {
	
	private int status;
	
	private String email;
	
	private Workflow workflow;
	
	public Observador(AbstractFactory factory) {
		this.email = factory.createEmail();
		this.workflow = factory.createWorkflow();
	}
	
	public abstract void update(int status);

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}
	
}
