package com.software;

import java.util.concurrent.TimeUnit;

public class Workflow {
	
	public void executar() throws InterruptedException {	
		System.out.println("Evacue o andar");
		TimeUnit.SECONDS.sleep(2);
		System.out.println("Cheque o sistema de arrefecimento");
		TimeUnit.SECONDS.sleep(2);
		System.out.println("Espere um pouco");
		TimeUnit.SECONDS.sleep(2);
		System.out.println("Cheque a temperatura do andar e espere mais um pouco");
		TimeUnit.SECONDS.sleep(2);
		System.out.println("Ambiente controlado");
	}

}
