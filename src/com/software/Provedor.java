package com.software;
import java.util.ArrayList;
import java.util.List;

public abstract class Provedor {

	private List<Observador> inscritos;
	
	public Provedor() {}
	
	public void subscribe(Observador obs) {
		if(inscritos == null) {
			inscritos = new ArrayList<Observador>();
		}
		inscritos.add(obs);
	}
	
	public void unsubscribe(Observador obs) {
		if(inscritos.contains(obs)) {
			inscritos.remove(obs);
		}
	}
	
	public abstract void notificar(Observador obs, int temperatura);
	
//	Getters and Setters

	public List<Observador> getInscritos() {
		return inscritos;
	}

	public void setInscritos(List<Observador> inscritos) {
		this.inscritos = inscritos;
	}
	
}
