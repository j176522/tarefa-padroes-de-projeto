package com.software;

public class Sensor extends Provedor {
	
	private int temperatura;
	
	public int getTemperatura() {
		return temperatura;
	}
	
	public void setTemperatura(int temperatura) {
		this.temperatura = temperatura;
		for (Observador obs : getInscritos()) {
			notificar(obs, temperatura);
		}
	}
	
	public void notificar(Observador obs, int temperatura) {
		obs.update(temperatura);
	}
}
