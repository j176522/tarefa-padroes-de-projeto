package com.software;

public class ActionHandler extends Observador {
	
	public ActionHandler(AbstractFactory factory) {
		super(factory);
	}
	
	public void update(int status) {
		setStatus(status);
		if(status > 40 || status < 10) {
			try {
				getWorkflow().executar();
			} catch (InterruptedException e) {
				
			}
		} else if(status > 30 || status < 20) {
			System.out.println(getEmail());
		}
	}
}
