package com.software;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Sensor sensor = new Sensor();
		Scanner scan = new Scanner(System.in);
		String input;
		int temp;
		ActionHandler obs1 = new ActionHandler(new FabricaConcreta());
		ActionHandler obs2 = new ActionHandler(new FabricaConcreta());
		ActionHandler obs3 = new ActionHandler(new FabricaConcreta());
		sensor.subscribe(obs1);
		sensor.subscribe(obs2);
		sensor.subscribe(obs3);
		sensor.unsubscribe(obs2);
		
		System.out.println("Digite as temperaturas (caso queira sair digite exit)");
		while(true) {
			input = scan.nextLine();
			if(input.toLowerCase().equals("exit")) {
				break;
			} else {
				try{
					temp = Integer.parseInt(input);
					sensor.setTemperatura(temp);
				} catch(NumberFormatException e) {
					System.out.println("Entrada invalida");
				} finally {
					System.out.println("Digite uma nova temperatura (caso queira sair digite exit)");
				}
			}
		}
		
		scan.close();
		
		System.out.println("Sistema fechado");
	}

}
